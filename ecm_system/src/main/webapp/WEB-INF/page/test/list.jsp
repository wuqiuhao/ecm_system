<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>测评管理</title>
</head>
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <div style="margin: 10px">
            <form class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">试卷名</label>
                        <div class="layui-input-inline">
                            <input type="text" name="paperName" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn layui-btn-primary" lay-submit lay-filter="search-btn"><i
                                class="layui-icon"></i> 搜 索
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
    </div>
</div>
<script type="text/html" id="barDemo">
    {{#  if(d.scorePaper == null || d.scorePaper == ""){ }}
    <a class="layui-btn layui-btn-xs" lay-event="insert">进入考试</a>
    {{#  } else { }}
    <a class="layui-btn layui-btn-xs" aria-disabled="true">已完成考试</a>
    {{#  } }}

</script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: 'test/queryPaperByStuId',
            contentType: 'application/json',
            method: "post",
            toolbar: '#toolbar',
            defaultToolbar: ['filter', 'exports', 'print'],
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID'},
                {field: 'paperName',width: 180, title: '试卷名'},
                {field: 'courseName',width: 180, title: '课程名'},
                {field: 'year', title: '年份'},
                {field: 'type', title: '类型'},
                {field: 'sectionId', title: '选课ID', hide: true},
                {field: 'courseId', title: '课程ID', hide: true},
                {field: 'startTime', title: '开始时间'},
                {field: 'endTime', title: '结束时间'},
                {title: '操作', toolbar: '#barDemo', width: 300}
            ]],
            skin: 'line'
        });
        // 监听搜索操作
        form.on('submit(search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                contentType: 'application/json',
                where: data.field
            }, 'data');
            return false;
        });
        /**
         * toolbar事件监听
         */
        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            paper_id = data.id;
            paper_name = data.paperName;
            section_id = data.sectionId;
            course_id = data.courseId;
            if (obj.event === 'insert') {   // 监听添加操作
                layer.confirm('确定进入考试？', function (index) {
                    layer.msg('即将进入考试，1秒后自动关闭该窗口');
                    //延迟1秒执行，目的是让用户看到提示
                    setTimeout(function () {
                        var index = layer.open({
                            title: '正在考试中，请勿退出...',
                            type: 2,
                            shade: 0.2,
                            shadeClose: false,
                            area: ['80%', '90%'],
                            content: 'test/testing',
                            end: function () {
                                table.reload('currentTableId');
                            }
                        });
                    }, 1 * 1000);
                    layer.close(index);
                });
            }
        });
    });
</script>
</body>
</html>