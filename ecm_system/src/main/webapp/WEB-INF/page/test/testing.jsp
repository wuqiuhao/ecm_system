<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>正在考试</title>
</head>
<body style="height: 100%">
<form class="layui-form" action="" lay-filter="example">
    <div class="layuimini-container layuimini-page-anim">
        <div class="layui-container">
            <div class="layuimini-row">
                <div class="layuimini-col-8">
                    <h3 style="text-align: center"><span id="paperName"></span></h3>
                    <span style="float: right;color: red" id="timer"></span>
                </div>
            </div>
        </div>
        <div class="layuimini-main">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
                <div style="margin: 10px" id="question">
                    <div class="layui-input-block" id="subject" style="margin-top: 20px">

                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="layui-form-item" style="text-align: center">
        <div class="layui-input-block">
            <button lay-filter="tijiao" type="button" class="btn btn-sm layui-btn-primary" lay-submit>立即提交</button>
        </div>
    </div>
</form>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form
        paperId = parent.paper_id;
        paperName = parent.paper_name;
        sectionId = parent.section_id;
        courseId = parent.course_id;
        $("#paperName").text(paperName);

        $(function () {
            TimeDown("timer", 1800000)
        });

        var index=parent.layer.getFrameIndex(window.name);
        $.ajax({
            url: "${basePath}test/getRandTopic",
            type: "GET",
            dataType: 'json',
            data: "paperId=" + paperId,
            success: function (data) {
                var str = '';
                for (var i = 0; i < data.length; i++) {
                    var name = data[i]["quesName"];
                    var answer = data[i]["quesAnswer"];
                    var a = data[i]["quesA"];
                    var b = data[i]["quesB"];
                    var c = data[i]["quesC"];
                    var d = data[i]["quesD"];
                    str = '<h4 style="margin-left: 5px;"><span style="margin-left: 5px;" id=' + "quesName" + i + '></span></h4>' +
                        '<span id=' + "quesAnswer" + i + ' style="display: none"></span>' +
                        '<span id="quesScore" style="float: right">分值:10</span>' +
                        '<input type="radio" name=' + "cheakRadios" + i + ' value="A"><span style="margin-left: 5px;" id=' + "quesA" + i + '></span><br>' +
                        '<input type="radio" name=' + "cheakRadios" + i + ' value="B"><span style="margin-left: 5px;" id=' + "quesB" + i + '></span><br>' +
                        '<input type="radio" name=' + "cheakRadios" + i + ' value="C"><span style="margin-left: 5px;" id=' + "quesC" + i + '></span><br>' +
                        '<input type="radio" name=' + "cheakRadios" + i + ' value="D"><span style="margin-left: 5px;" id=' + "quesD" + i + '></span><br>';
                    $("#subject").append(str);
                    $("#quesName" + i).text(i + 1 + '.' + name);
                    $("#quesAnswer" + i).text(answer);
                    $("#quesA" + i).text('A.' + a);
                    $("#quesB" + i).text('B.' + b);
                    $("#quesC" + i).text('C.' + c);
                    $("#quesD" + i).text('D.' + d);
                }
            }
        });
        form.on('submit(tijiao)', function (data) {
            layer.confirm('确定提交试卷？', function () {
                debugger;
                var score = 0;
                for (var k = 0; k <= 9; k++){
                    var zhuangtai = 0;
                    var xuan = $("input[name='cheakRadios"+k+"']");
                    for (var i = 0;i<xuan.length; i++){
                        var result = $("#quesAnswer" + k).text();
                        if (xuan[i].checked == true){
                            zhuangtai = 1;
                            if (result == xuan[i].value){
                                score = score +10;
                            }
                            break;
                        }else {

                        }
                    }
                    if (zhuangtai == 0){
                        layer.msg("您有未做完的题，请仔细检查！");
                    }
                }
                // for (var i = 0; i < 10; i++) {
                //     debugger;
                //     var answer = 'cheakRadios' + i;
                //     //首先判断是否有未选中的
                //     var b = document.getElementsByName(answer);
                //     var a1 = $("#quesAnswer" + i).text();
                //     for (var j = 0; j < b.length; j++) {
                //         if (b[j].checked = true) {
                //             if (b[j].value == a1) {
                //                 score = score + 10;
                //             }
                //     }else if (b[j].checked = false){
                //             layer.msg("您有未做完的题，请仔细检查！");
                //         }
                //     }
                // };
                //提交成绩
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "${basePath}score/insertScore",
                    data: {
                        "scorePaper": score,
                        "sectionId": sectionId,
                        "courseId": courseId
                    },
                    success: function (result) {
                        if (result.code === "1000") {
                            layer.msg('交卷成功，1秒后自动关闭该窗口');
                            //延迟1秒执行，目的是让用户看到提示
                            setTimeout(function () {
                                parent.layer.close(index);//关闭窗口
                            }, 1 * 1000);
                        } else {
                            layer.msg("数据异常！");
                        }
                        ;
                    },
                    error: function () {
                        layer.msg('后台异常！');
                    }
                });
            })
            return false;
        })

    });
</script>
<script>
    function TimeDown(id, value) {
        //倒计时的总秒数
        var totalSeconds = parseInt(value / 1000);
        //取模（余数）
        var modulo = totalSeconds % (60 * 60 * 24);
        //小时数
        var hours = Math.floor(modulo / (60 * 60));
        modulo = modulo % (60 * 60);
        //分钟
        var minutes = Math.floor(modulo / 60);
        //秒
        var seconds = modulo % 60;
        hours = hours.toString().length == 1 ? '0' + hours : hours;
        minutes = minutes.toString().length == 1 ? '0' + minutes : minutes;
        seconds = seconds.toString().length == 1 ? '0' + seconds : seconds;
        //输出到页面
        document.getElementById(id).innerHTML = "剩余时间:" + hours + ":" + minutes + ":" + seconds;
        //延迟一秒执行自己
        if (hours == "00" && minutes == "00" && parseInt(seconds) - 1 < 0) {
        } else {
            setTimeout(function () {
                TimeDown(id, value - 1000);
            }, 1000)
        }
    }
</script>
</body>
</html>