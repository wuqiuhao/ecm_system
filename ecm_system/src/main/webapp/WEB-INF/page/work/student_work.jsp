<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_100">
        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
        <input type="hidden" id="courseId" name="courseId" value="${course.id}">
        <input type="hidden" id="workId" name="courseId" value="${course.id}">
    </div>
    <script type="text/html" id="barDemo">

        <a class="layui-btn layui-btn-xs" lay-event="answer">答题</a>

        <a class="layui-btn layui-btn-xs" lay-event="score">查看得分</a>
    </script>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','upload','table'], function () {
        var form = layui.form,
            upload=layui.upload,
            $ = layui.jquery,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: 'queryWorkByCourseId/'+$("#courseId").val(),
            contentType:'application/json',
            method:"post",
            toolbar: '#toolbar',
            page: true,
            cols: [[
                {field: 'id', width: 80, title: 'ID'},
                {field: 'workTitle',  title: '作业题目'},
                {field: 'workFile',  title: '作业内容'},
                {field: 'startTime',  title: '作业开始时间'},
                {field: 'endTime',  title: '作业结束时间'},
                {title: '操作', toolbar: '#barDemo', width: 200}
            ]],
            skin: 'line'
        });
        //监听行工具事件
        table.on('tool(currentTableFilter)', function(obj) {
            var data = obj.data;
            var id = data.id;
            if (obj.event === 'answer') {
                $.ajax({
                    url:"${basePath}answer/check/"+id,
                    type:"POST",
                    contentType:'application/json',
                    success:function(data){
                        if (data['code'] == 1001){
                            var index = layer.open({
                                title: '作业详情',
                                type: 2,
                                shade: 0.2,
                                shadeClose: false,
                                area: ['90%', '90%'],
                                content: 'workDetail/' + id,
                                end: function () {
                                    table.reload('currentTableId');
                                }
                            });
                        }else {
                            layer.msg("已经作答，请勿重复答题！")
                            return false;
                        }
                    },
                    end:function(){
                        parent.table.reload('currentTableId');
                    }
                });

            }else if (obj.event === 'score'){
                $.ajax({
                    url:"${basePath}answer/checkScore/"+id,
                    type:"POST",
                    contentType:'application/json',
                    success:function(res) {
                        if (res['data'] == null ||res['data']['answerScore'] == null){
                            layer.msg("老师未批改，请稍后查看！")
                            return false;
                        }else {
                            var index = layer.open({
                                title: '作业得分详情',
                                type: 2,
                                shade: 0.2,
                                shadeClose: false,
                                area: ['90%', '90%'],
                                content: 'scoreDetail/' + id,
                                end: function () {
                                    table.reload('currentTableId');
                                }
                            });
                        }
                    }
                });
            }
        });
        //下载文件，调用后端接口
        function download(fileName,filePath){
            window.open("${basePath}file/downloadFile?fileName="+encodeURI(fileName)+"&fileUrl="+filePath);
            return ;
        }
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);
    });
</script>
</body>
</html>
