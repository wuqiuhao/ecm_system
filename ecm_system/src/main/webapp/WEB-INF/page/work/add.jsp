<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_60">
        <form class="layui-form">
            <input type="hidden" id="courseId" name="courseId">
            <div class="layui-form-item">
                <label class="layui-form-label">题目<span>*</span></label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="workTitle" id="txt" lay-verify="txt"></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">作业内容<span>*</span></label>
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-normal" id="test3">选择文件</button>
                    <input type="hidden" name="workFile" id="attach" class="layui-input" lay-verify="required">
                    <button type="button" class="layui-btn" id="test9">开始上传</button></br>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">作业开始时间<span>*</span></label>
                <div class="layui-input-block">
                    <input id="startTime" type="text" name="startTime" class="layui-input" lay-verify="required" placeholder="yyyy-MM-dd">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">作业截至时间<span>*</span></label>
                <div class="layui-input-block">
                    <input id="endTime" type="text" name="endTime" class="layui-input" lay-verify="required" placeholder="yyyy-MM-dd">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">课程<span>*</span></label>
                <div class="layui-input-block">
                    <select name="courseId" lay-filter="eventName" id="course" class="layui-input newclassname">
                        <option value="" selected>请选择课程</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-primary layui-btn-sm data-add-btn">
                        <i class="fa fa-refresh"></i>
                        重置
                    </button>
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
                        <i class="fa fa-save"></i>
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','laydate','upload','util','layedit'], function () {
        var form = layui.form,$ = layui.jquery,upload=layui.upload,
        laydate = layui.laydate;
        var layedit = layui.layedit;
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);
        //日期选择器
        laydate.render({
            elem: '#startTime'
        })
        laydate.render({
            elem: '#endTime'
        })
        //
        $.ajax({
            type: "POST",
            url: '${basePath}work/queryCourse',
            success: function (res) {
                if(res.data){
                    var optionHtml = '';
                    for(var i in res.data){
                        var value1 = res.data[i]['id'];
                        var value2 = res.data[i]['courseName'];
                        optionHtml += "<option value='" + value1 + "'>" + value2 + "</option>";
                    }
                    $('.newclassname').append(optionHtml);
                    layui.form.render('select');
                }
            }
        });
        var uploadInst = upload.render({
            elem: '#test3'
            ,url: '${basePath}file/uploadFile' //改成您自己的上传接口
            ,accept: 'file' //普通文件
            ,auto:false
            ,bindAction:'#test9'
            ,done: function(res){
                $("#attach").val(res.data);
                layer.msg('上传成功');
                console.log(res);
            }
        });
        //监听提交
        form.on('submit(save)', function (data) {
            var json = {};
            json.workTitle = data.field.workTitle;
            json.workFile = data.field.workFile;
            json.startTime = data.field.startTime;
            json.endTime = data.field.endTime;
            json.courseId = $("#course").val();
            $.ajax({
                url:"${basePath}work/create",
                type:"POST",
                contentType:'application/json',
                dataType:'json',
                data:JSON.stringify(json),
                success:function(data){
                    layer.msg(data.msg,{time:500},
                        function(){
                            parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
