<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_100">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>作业内容</legend>
        </fieldset>
            <input type="hidden" id="course" value="${work.courseId}">
            <input type="hidden" id="workId" value="${work.id}">
            <div class="layui-form-item">
                <label class="layui-form-label">题目<span>*</span></label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="workTitle" readonly id="txt"  lay-verify="txt">${work.workTitle}</textarea>
                </div>
            </div>
        <div class="layui-inline">
            <label class="layui-form-label">作业附件<span>*</span></label>
            <div class="layui-input-inline">
                <input type="text" style="width:200px;" name="workFile" id="attach" class="layui-input" value="${work.workFile}" readonly>
            </div>
        </div>
        <div class="layui-inline">
            <button class="layui-btn layui-btn-danger" id="download">附件下载</button>
        </div>
        <form class="layui-form">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>请在下面输入你的答案</legend>
            </fieldset>
            <div class="layui-form-item">
                <label class="layui-form-label">答案<span>*</span></label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="answer" id="answer"></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">上传作业相关文件</label>
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-normal" id="test3">选择文件</button>
                    <input type="hidden" name="answerFile" id="answerFile" class="layui-input" lay-verify="required">
                    <button type="button" class="layui-btn" id="test9">开始上传</button></br>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-primary layui-btn-sm data-add-btn">
                        <i class="fa fa-refresh"></i>
                        重置
                    </button>
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
                        <i class="fa fa-save"></i>
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','laydate','upload','util','layedit'], function () {
        var form = layui.form,$ = layui.jquery,upload=layui.upload,
            laydate = layui.laydate;
        var layedit = layui.layedit;
        var util = layui.util;
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);

        var uploadInst = upload.render({
            elem: '#test3'
            ,url: '${basePath}file/uploadFile' //改成您自己的上传接口
            ,accept: 'file' //普通文件
            ,auto:false
            ,bindAction:'#test9'
            ,done: function(res){
                $("#answerFile").val(res.data);
                layer.msg('上传成功');
                console.log(res);
            }
        });
        //附件下载
        $(document).on('click','#download',function(){
            download("作业",$("#attach").val());
         });
        //下载文件，调用后端接口
        function download(fileName,filePath){
            window.open("${basePath}file/downloadFile?fileName="+encodeURI(fileName)+"&fileUrl="+filePath);
            return ;
        }
        //监听提交
        form.on('submit(save)', function (data) {
            var json = {};
            json.answer = data.field.answer;
            json.answerFile = data.field.answerFile;
            json.workId = $("#workId").val();
            $.ajax({
                url:"${basePath}answer/create",
                type:"POST",
                contentType:'application/json',
                dataType:'json',
                data:JSON.stringify(json),
                success:function(data){
                    layer.msg(data.msg,{time:500},
                        function(){
                            parent.layer.close(index);
                        });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>