<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_100">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>课程作业</legend>
        </fieldset>
        <div class="layui-form-item">
            <label class="layui-form-label">题目</label>
            <div class="layui-input-block">
                <textarea class="layui-textarea" name="workTitle" readonly id="txt"  lay-verify="txt">${answer.workTitle}</textarea>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">作业附件</label>
            <div class="layui-input-inline">
                <input type="text" style="width:200px;" name="workFile" id="attach" class="layui-input" value="${answer.workFile}" readonly>
            </div>
        </div>
        <div class="layui-inline">
            <button class="layui-btn layui-btn-danger" id="download">下载</button>
        </div>

        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>学生答案</legend>
        </fieldset>
            <div class="layui-form-item">
                <label class="layui-form-label">答案</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="answer" id="answer">${answer.answer}</textarea>
                </div>
            </div>
        <div class="layui-inline">
            <label class="layui-form-label">答案附件</label>
            <div class="layui-input-inline">
                <input type="text" style="width:200px;" name="answerFile" id="attach1" class="layui-input" value="${answer.answerFile}" readonly>
            </div>
        </div>
        <div class="layui-inline">
            <button class="layui-btn layui-btn-danger" id="download1">下载</button>
        </div>
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
            <legend>教师评分</legend>
        </fieldset>
        <form class="layui-form">
            <input type="hidden" id="answerId" value="${answer.id}">
            <input type="hidden" id="stuName" value="${answer.stuName}">
            <div class="layui-inline">
                <label class="layui-form-label">分数<span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" style="width:200px;" name="answerScore" id="answerScore"  class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
                        <i class="fa fa-save"></i>
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','table','jquery','laydate','upload','util','layedit'], function () {
        var form = layui.form,$ = layui.jquery,upload=layui.upload,table=layui.table,
            laydate = layui.laydate;
        var layedit = layui.layedit;
        var util = layui.util;
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);

        var uploadInst = upload.render({
            elem: '#test3'
            ,url: '${basePath}file/uploadFile' //改成您自己的上传接口
            ,accept: 'file' //普通文件
            ,auto:false
            ,bindAction:'#test9'
            ,done: function(res){
                $("#answerFile").val(res.data);
                layer.msg('上传成功');
                console.log(res);
            }
        });
        //附件下载
        $(document).on('click','#download',function(){
            download("作业",$("#attach").val());
        });
        $(document).on('click','#download1',function(){
            download($("#stuName").val()+"-答案",$("#attach1").val());
        });
        //下载文件，调用后端接口
        function download(fileName,filePath){
            window.open("${basePath}file/downloadFile?fileName="+encodeURI(fileName)+"&fileUrl="+filePath);
            return ;
        }
        //监听提交
        form.on('submit(save)', function (data) {
            var json = {};
            json.id = $("#answerId").val();
            json.answerScore = $("#answerScore").val();
            $.ajax({
                url:"${basePath}answer/score",
                type:"POST",
                contentType:'application/json',
                dataType:'json',
                data:JSON.stringify(json),
                success:function(data){
                    layer.msg(data.msg,{time:500},
                        function(){
                            //找到它的子窗口的body
                            var body = layer.getFrameIndex('body', index);  //巧妙的地方在这里哦
                            //为子窗口元素赋值
                            body.contents().find("#courseId").val(courseId);
                            parent.layer.close(index);
                        });
                },
                end:function(){
                    parent.table.reload('currentTableId');
                }
            });
            return false;
        });
    });
</script>
</body>
</html>