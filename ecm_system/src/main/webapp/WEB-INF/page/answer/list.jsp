<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <input type="hidden" id="courseId" name="courseId" value="${course.id}">
        <input type="hidden" id="workId" name="courseId">
        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
    </div>
    <script type="text/html" id="barDemo">
        {{#  if(d.answerScore == null){ }}
        <a class="layui-btn layui-btn-xs " lay-event="audit">批改作业</a>
        {{#  } else{ }}
        <a class="layui-btn layui-btn-xs layui-btn-disabled">已批改</a>
        {{#  } }}
    </script>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: 'queryAnswerByWorkId/'+$("#workId").val(),
            contentType:'application/json',
            method:"post",
            toolbar: '#toolbar',
            defaultToolbar: ['filter', 'exports', 'print'],
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID'},
                {field: 'stuName',  title: '学生姓名'},
                {field: 'answer',  title: '学生答案'},
                {field: 'answerFile',  title: '答案附件'},
                {field: 'answerScore',  title: '分数',templet: function (d) {
                        if(d.answerScore == null){
                            res = "未批改";
                        }else {
                            res = d.answerScore;
                        }
                        return res;
                    }},
                {title: '操作', toolbar: '#barDemo', width: 200}
            ]],
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                contentType:'application/json',
                where: data.field
            }, 'data');
            return false;
        });
        //监听行工具事件
        table.on('tool(currentTableFilter)', function(obj) {
            var data = obj.data;
            var answerId = data.id;
            if (obj.event === 'audit') {
                var index = layer.open({
                    title: '学生作业列表',
                    type: 2,
                    shade: 0.2,
                    shadeClose: false,
                    area: ['90%', '90%'],
                    content: 'detail/'+answerId,
                    end:function(){
                        table.reload('currentTableId');
                    }
                });
            }
        });
    });
</script>
</body>
</html>
