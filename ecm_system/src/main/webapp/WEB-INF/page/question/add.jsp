<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_60">
        <form class="layui-form">
            <div class="layui-form-item">
                <label class="layui-form-label">题目</label>
                <div class="layui-input-block">
                    <textarea name="quesName" class="layui-textarea" lay-verify="content" placeholder="请输入题目"></textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项A</label>
                <div class="layui-input-block">
                    <input id="quesA" type="text" name="quesA" class="layui-input" lay-verify="required" placeholder="请输入选项内容">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项B</label>
                <div class="layui-input-block">
                    <input id="quesB" type="text" name="quesB" class="layui-input" lay-verify="required" placeholder="请输入选项内容">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项C</label>
                <div class="layui-input-block">
                    <input id="quesC" type="text" name="quesC" class="layui-input" lay-verify="required" placeholder="请输入选项内容">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">选项D</label>
                <div class="layui-input-block">
                    <input id="quesD" type="text" name="quesD" class="layui-input" lay-verify="required" placeholder="请输入选项内容">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">答案</label>
                <div class="layui-input-block">
                    <select id="quesAnswer" name="quesAnswer">
                        <option value="">请选择</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">试卷</label>
                <div class="layui-input-block">
                    <select id="paperId" name="paperId">
                        <option value="">请选择</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-primary layui-btn-sm data-add-btn">
                        <i class="fa fa-refresh"></i>
                        重置
                    </button>
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
                        <i class="fa fa-save"></i>
                        保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','laydate'], function () {
        var form = layui.form,$ = layui.jquery;
        var laydate = layui.laydate;
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);

        //获取当前登录教师试题列表
        $.ajax({
            url: '${basePath}question/queryPaperById',
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data, function (index, value) {
                    $('#paperId').append(new Option(value.paperName,value.id));// 下拉菜单里添加元素
                    $('#paperId').find('option').each(function() {
                    //     $(this).attr('selected', $(this).val() == departmentID);
                     });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //监听提交
        form.on('submit(save)', function (data) {
            $.ajax({
                url:"${basePath}question/create",
                type:"POST",
                contentType:'application/json',
                dataType:'json',
                data:JSON.stringify(data.field),
                success:function(data){
                    layer.msg(data.msg,{time:500},
                        function(){
                            parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
