<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_60">
        <form class="layui-form">
            <div class="layui-form-item">
                <label class="layui-form-label">试卷名</label>
                <div class="layui-input-block">
                    <input type="text" name="paperName" lay-verify="required" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">课程</label>
                <div class="layui-input-block">
                    <select name="sectionId" id="sectionId">
                        <option value="">请选择课程</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">开始时间</label>
                <div class="layui-input-block">
                    <input id="startTime" type="text" name="startTime" class="layui-input" lay-verify="required" placeholder="yyyy-MM-dd">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">结束时间</label>
                <div class="layui-input-block">
                    <input id="endTime" type="text" name="endTime" class="layui-input" lay-verify="required" placeholder="yyyy-MM-dd">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-primary layui-btn-sm data-add-btn">
                        <i class="fa fa-refresh"></i>
                        重置
                    </button>
                    <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
                        <i class="fa fa-save"></i>
                        发布试卷
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','laydate'], function () {
        var form = layui.form,$ = layui.jquery;
        var laydate = layui.laydate;
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);
        //日期选择器
        laydate.render({
            elem: '#startTime'
        })
        laydate.render({
            elem: '#endTime'
        })
        //获取当前登录教师课程列表
        $.ajax({
            url: '${basePath}paper/querySectionById',
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data, function (index, value) {
                    $('#sectionId').append(new Option(value.courseName,value.id));// 下拉菜单里添加元素
                    $('#sectionId').find('option').each(function() {
                    //     $(this).attr('selected', $(this).val() == departmentID);
                     });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //监听提交
        form.on('submit(save)', function (data) {
            $.ajax({
                url:"${basePath}paper/create",
                type:"POST",
                contentType:'application/json',
                dataType:'json',
                data:JSON.stringify(data.field),
                success:function(data){
                    layer.msg(data.msg,{time:500},
                        function(){
                            parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
