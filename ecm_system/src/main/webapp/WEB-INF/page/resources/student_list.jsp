<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>课程资源管理</title>
</head>
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main">
        <div style="margin: 10px">
            <form class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">课程名</label>
                        <div class="layui-input-inline">
                            <input type="text" name="courseName" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn layui-btn-primary"  lay-submit lay-filter="search-btn"><i class="layui-icon"></i> 搜 索</button>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/html" id="toolbar">
            <a class="layui-btn layui-btn-xs " lay-event="detail">查看课程资源</a>
            <a class="layui-btn layui-btn-xs layui-btn-danger " lay-event="work">查看课程作业</a>
        </script>
        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
    </div>
</div>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: 'resources/queryByStudentId',
            contentType:'application/json',
            method:"post",
            defaultToolbar: ['filter', 'exports', 'print'],
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID'},
                {field: 'courseName',  title: '课程名称'},
                {field: 'courseRoom',  title: '课程教室'},
                {field: 'teacherName',  title: '任课教师'},
                {field: 'remark',  title: '课程简介'},
                {title: '操作', toolbar: '#toolbar',width:280}
            ]],
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(search-btn)', function (data) {
            //执行搜索重载
            table.reload('currentTableId', {
                contentType:'application/json',
                where: data.field
            }, 'data');
            return false;
        });
        /**
         * toolbar事件监听
         */
        table.on('tool(currentTableFilter)', function (obj) {
            var data = obj.data;
            var courseId = data.id;
            if (obj.event === 'detail') {   // 监听添加操作
                var index = layer.open({
                    title: '课程资源列表',
                    type: 2,
                    shade: 0.2,
                    shadeClose: false,
                    area: ['80%', '80%'],
                    content: 'resources/student_resources',
                    success: function (layero, index) {
                        //找到它的子窗口的body
                        var body = layer.getChildFrame('body', index);  //巧妙的地方在这里哦
                        //为子窗口元素赋值
                        body.contents().find("#courseId").val(courseId);
                    },
                    end: function () {
                        table.reload('currentTableId');
                    }
                });
            }else if (obj.event === 'work'){
                var index = layer.open({
                    title: '课程作业列表',
                    type: 2,
                    shade: 0.2,
                    shadeClose: false,
                    area: ['100%', '100%'],
                    content: 'work/student_work',
                    success: function (layero, index) {
                        //找到它的子窗口的body
                        var body = layer.getChildFrame('body', index);  //巧妙的地方在这里哦
                        //为子窗口元素赋值
                        body.contents().find("#courseId").val(courseId);
                    },
                    end: function () {
                        table.reload('currentTableId');
                    }
                });
            }
        });
    });
</script>
</body>
</html>
