<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
  <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
  <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
  <div class="layuimini-main width_100">
    <div style="margin: 10px">
      <form class="layui-form layui-form-pane">
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">课程名</label>
            <div class="layui-input-inline">
              <select id="fileType" name="fileType">
                <option value="">请选择资源类型</option>
                <option value="1">课程PPT</option>
                <option value="2">教案</option>
                <option value="3">课程视频</option>
                <option value="4">其他</option>
              </select>
            </div>
          </div>
          <div class="layui-inline">
            <button class="layui-btn layui-btn-primary reset"><i class="fa fa-refresh"></i>重置</button>
            <button class="layui-btn layui-btn-primary"  lay-submit lay-filter="search-btn"><i class="layui-icon"></i> 搜 索</button>
          </div>
        </div>
      </form>
    </div>
    <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
    <form class="layui-form">
      <input type="hidden" id="courseId" name="courseId" value="${course.id}">
      <div class="layui-form-item">
      </div>
      <div class="layui-form-item">
      </div>
      <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
        <legend>上传课程资源（课件、视频等）</legend>
      </fieldset>
      <div class="layui-form-item">
        <label class="layui-form-label">资源名称</label>
          <div class="layui-input-block">
            <input type="text" name="fileName" id="fileName" class="layui-input" lay-verify="required">
          </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">课程资源</label>
        <div class="layui-upload">
          <button type="button" class="layui-btn layui-btn-normal" id="test3">选择文件</button>
          <input type="hidden" name="attach" id="attach" class="layui-input" lay-verify="required">
          <button type="button" class="layui-btn" id="test9">开始上传</button></br>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">资源类型</label>
        <div class="layui-input-block">
        <select name="fileType" lay-verify="required">
          <option value="">请选择资源类型</option>
          <option value="1">课程PPT</option>
          <option value="2">教案</option>
          <option value="3">课程视频</option>
          <option value="4">其他</option>
        </select>
        </div>
      </div>
      <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block">
          <input type="text" name="remarks" id="remarks" class="layui-input">
        </div>
      </div>
      <div class="layui-form-item">
        <div class="layui-input-block">
          <button class="layui-btn layui-btn-primary layui-btn-sm data-add-btn reset" >
            <i class="fa fa-refresh"></i>
            重置
          </button>
          <button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-submit lay-filter="save">
            <i class="fa fa-save"></i>
            保存
          </button>
        </div>
      </div>
    </form>
  </div>
  <script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs " lay-event="download">下载</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>
  </script>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
  layui.use(['form','jquery','upload','table'], function () {
    var form = layui.form,
            upload=layui.upload,
            $ = layui.jquery,
            table = layui.table;
    table.render({
      elem: '#currentTableId',
      url: 'queryInfoByCourseId/'+$("#courseId").val(),
      contentType:'application/json',
      method:"post",
      toolbar: '#toolbar',
      page: true,
      cols: [[
        {type: "checkbox", width: 50},
        {field: 'id', width: 80, title: 'ID'},
        {field: 'fileName',  title: '文件名称'},
        {field: 'createTime',  title: '上传时间'},
        {field: 'fileType',  title: '资源类型',templet: function (d) {
            if(d.fileType == 1){
              res = "课程PPT"
            }else if(d.fileType == 2){
              res = "教案"
            }else if (d.fileType == 3){
              res = "课程视频"
            }else if (d.fileType == 4){
              res = "其他"
            }
            return res;
          }},
        {field: 'remarks',title: '备注'},
        {title: '操作', toolbar: '#barDemo', width: 200}
      ]],
      skin: 'line'
    });
// 监听搜索操作
    form.on('submit(search-btn)', function (data) {
      //执行搜索重载
      table.reload('currentTableId', {
        contentType:'application/json',
        where: data.field
      }, 'data');
      return false;
    });
    var uploadInst = upload.render({
      elem: '#test3'
      ,url: '${basePath}file/uploadFile' //改成您自己的上传接口
      ,accept: 'file' //普通文件
      ,auto:false
      ,bindAction:'#test9'
      ,done: function(res){
        $("#attach").val(res.data);
        layer.msg('上传成功');
        console.log(res);
      }
    });
//监听行工具事件
    table.on('tool(currentTableFilter)', function(obj) {
      var data = obj.data;
      id = data.id;
      fileName = data.fileName;
      fileUrl = data.fileUrl;
      resourseId = data.id;
      if (obj.event === 'delete') {
        layer.confirm('真的删除行么', function (index) {
          $.ajax({
            url:"${basePath}resources/delete/"+resourseId,
            type:"POST",
            contentType:'application/json',
            dataType:'json',
            success:function(data){
              layer.msg(data.msg,{time:500},
                      function(){
                        table.reload("currentTableId");
                      });
            }
          });
        })
      }else if (obj.event === 'download') {
        $.ajax({
          url:"${basePath}resources/getProjectFiles/"+id,
          type:"POST",
          dataType: "JSON",
          success: function (ret) {
            if (ret['code'] == 1000) {
              debugger
              download(ret['fileName'],ret['fileUrl']);
            }
          }
        });
      }
    });
    //下载文件，调用后端接口
    function download(fileName,filePath){
      window.open("${basePath}file/downloadFile?fileName="+encodeURI(fileName)+"&fileUrl="+filePath);
      return ;
    }
    var index = parent.layer.getFrameIndex(window.name);
    //监听提交
    form.on('submit(save)', function (data) {
      var json = {};
      json.fileName = data.field.fileName;
      json.fileUrl = data.field.attach;
      json.fileType = data.field.fileType;
      json.remarks = data.field.remarks;
      json.courseId = $("#courseId").val();
      $.ajax({
        url:"${basePath}resources/create",
        type:"POST",
        contentType:'application/json',
        dataType:'json',
        data:JSON.stringify(json),
        success:function(data){
          layer.msg(data.msg,{time:500},
                  function(){
                    table.reload("currentTableId");
                    $('.reset').click();
          });
        }
      });
      return false;
    });
  });
</script>
</body>
</html>
