<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${basePath}static/lib/layui-src/css/layui.css" media="all">
    <link rel="stylesheet" href="${basePath}static/lib/font-awesome-4.7.0/css/font-awesome.min.css" media="all">
    <link rel="stylesheet" href="${basePath}static/css/style.css" media="all">
<body>
<div class="layuimini-container layuimini-page-anim">
    <div class="layuimini-main width_100">
        <div style="margin: 10px">
            <form class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">资源类型</label>
                        <div class="layui-input-inline">
                            <select id="fileType" name="fileType">
                                <option value="">请选择</option>
                                <option value="1">课程PPT</option>
                                <option value="2">教案</option>
                                <option value="3">课程视频</option>
                                <option value="4">其他</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn layui-btn-primary reset"><i class="fa fa-refresh"></i>重置</button>
                        <button class="layui-btn layui-btn-primary"  lay-submit lay-filter="search-btn"><i class="layui-icon"></i> 搜 索</button>
                    </div>
                </div>
            </form>
        </div>
        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
        <input type="hidden" id="courseId" name="courseId" value="${course.id}">
    </div>
    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs " lay-event="download">下载</a>
    </script>
</div>
<script src="${basePath}static/lib/layui-src/layui.js" charset="utf-8"></script>
<script src="${basePath}static/js/lay-config.js?v=2.0.0" charset="utf-8"></script>
<script>
    layui.use(['form','jquery','upload','table'], function () {
        var form = layui.form,
            upload=layui.upload,
            $ = layui.jquery,
            table = layui.table;
        table.render({
            elem: '#currentTableId',
            url: 'queryInfoByCourseId/'+$("#courseId").val(),
            contentType:'application/json',
            method:"post",
            toolbar: '#toolbar',
            page: true,
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID'},
                {field: 'fileName',  title: '文件名称'},
                {field: 'createTime',  title: '上传时间'},
                {field: 'fileType',  title: '资源类型',templet: function (d) {
                        if(d.fileType == 1){
                            res = "课程PPT"
                        }else if(d.fileType == 2){
                            res = "教案"
                        }else if (d.fileType == 3){
                            res = "课程视频"
                        }else if (d.fileType == 4){
                            res = "其他"
                        }
                        return res;
                    }},
                {field: 'remarks',title: '备注'},
                {title: '操作', toolbar: '#barDemo', width: 200}
            ]],
            skin: 'line'
        });
        //监听行工具事件
        table.on('tool(currentTableFilter)', function(obj) {
            var data = obj.data;
            id = data.id;
            fileName = data.fileName;
            fileUrl = data.fileUrl;
            resourseId = data.id;
            if (obj.event === 'download') {
                $.ajax({
                    url:"${basePath}resources/getProjectFiles/"+id,
                    type:"POST",
                    dataType: "JSON",
                    success: function (ret) {
                        if (ret['code'] == 1000) {
                            debugger
                            download(ret['fileName'],ret['fileUrl']);
                        }
                    }
                });
            }
        });
        //下载文件，调用后端接口
        function download(fileName,filePath){
            window.open("${basePath}file/downloadFile?fileName="+encodeURI(fileName)+"&fileUrl="+filePath);
            return ;
        }
        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);
    });
</script>
</body>
</html>
