package com.zqy.entity;

import com.zqy.utils.Entity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/23/19:09
 * @Description:
 */
public class Work extends Entity {

    private Integer id;

    private String workTitle;

    private String workFile;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:SS")
    private Date startTime;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:SS")
    private Date endTime;

    private Integer courseId;

    private Integer teacherId;

    private String courseName;

    private Integer studentId;

    private Integer answered;

    public Integer getAnswered() {
        return answered;
    }

    public void setAnswered(Integer answered) {
        this.answered = answered;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getWorkFile() {
        return workFile;
    }

    public void setWorkFile(String workFile) {
        this.workFile = workFile;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getWorkTitle() {
        return workTitle;
    }

    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle;
    }

    public Integer getCourseId() {
        return courseId;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }
}
