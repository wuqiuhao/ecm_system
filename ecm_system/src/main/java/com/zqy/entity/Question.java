package com.zqy.entity;

import com.zqy.utils.Entity;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/21/14:04
 * @Description:
 */
public class Question extends Entity {

    private Integer id;

    private String quesName;

    private String quesA;

    private String quesB;

    private String quesC;

    private String quesD;

    private String quesAnswer;

    private int paperId;

    private Integer teacherId;

    private String courseName;

    private String paperName;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuesName() {
        return quesName;
    }

    public void setQuesName(String quesName) {
        this.quesName = quesName;
    }

    public String getQuesA() {
        return quesA;
    }

    public void setQuesA(String quesA) {
        this.quesA = quesA;
    }

    public String getQuesB() {
        return quesB;
    }

    public void setQuesB(String quesB) {
        this.quesB = quesB;
    }

    public String getQuesC() {
        return quesC;
    }

    public void setQuesC(String quesC) {
        this.quesC = quesC;
    }

    public String getQuesD() {
        return quesD;
    }

    public void setQuesD(String quesD) {
        this.quesD = quesD;
    }

    public String getQuesAnswer() {
        return quesAnswer;
    }

    public void setQuesAnswer(String quesAnswer) {
        this.quesAnswer = quesAnswer;
    }

    public int getPaperId() {
        return paperId;
    }

    public void setPaperId(int paperId) {
        this.paperId = paperId;
    }
}
