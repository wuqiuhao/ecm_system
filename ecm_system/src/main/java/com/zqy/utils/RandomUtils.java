package com.zqy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/22/11:03
 * @Description:
 */
public class RandomUtils {
    private static SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat sdf2=new SimpleDateFormat("yyyyMMddHHmmssSSS");
    private static Random random=new Random();

    /**
     * 得到当前日期，这个是FileController中给文件夹取名字的
     */
    public static String getCurrentDateForString() {
        return sdf1.format(new Date());
    }


    /**
     * 生成文件名使用时间+4位随机数
     * @param
     */
    public static String createFileNameUseTime(String fileName) {
        String fileSuffix=fileName.substring(fileName.lastIndexOf("."),fileName.length());
        String time=sdf2.format(new Date());
        Integer num=random.nextInt(9000)+1000;
        return time+num+fileSuffix;
    }

 }

