package com.zqy.utils;

import com.sun.javaws.Globals;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/22/11:02
 * @Description:
 */
public class AppFileUtils {

    /**
     * 得到文件上传的路径
     */
    public static String PATH="E:/upload/";
    static {
        //取到文件夹下的file.properties，也就是准备工作的file.properties文件，他是问见下载的路径
        InputStream stream = AppFileUtils.class.getClassLoader().getResourceAsStream("file.properties");
        //这个东西可厉害了，可以将file.properties文件下的内容path=E:/upload/封装成了键值对path：E:/upload/
        Properties properties=new Properties();
        try {
            properties.load(stream);
            //现在路径就是E:/upload/了
            PATH=properties.getProperty("path");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件下载
     * @param response
     * @param path
     * @param oldName
     * @return
     */
    public static ResponseEntity<Object> downloadFile(HttpServletResponse response, String path, String oldName) {
        //4,使用绝对路径+相对路径去找到文件对象
        File file=new File(AppFileUtils.PATH,path);
        //5,判断文件是否存在
        if(file.exists()) {
            try {
                //把file转成一个bytes
                byte [] bytes= FileUtils.readFileToByteArray(file);
                HttpHeaders header=new HttpHeaders();
                //封装响应内容类型(APPLICATION_OCTET_STREAM 响应的内容不限定)
                header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                //设置下载的文件的名称
                header.setContentDispositionFormData("attachment", oldName);
                //创建ResponseEntity对象

                ResponseEntity<Object> entity=
                        new ResponseEntity<Object>(bytes, header, HttpStatus.CREATED);
                System.err.println("文件下载成功！");
                return entity;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }else {
            PrintWriter out;
            try {
                out = response.getWriter();
                out.write("文件不存在");
                out.flush();
                out.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }

    //下载文件
    public static void downFile(String url, String filename, HttpServletResponse response) {
        File file=new File(AppFileUtils.PATH,url);
        response.setCharacterEncoding("utf-8");
        //response.setHeader("Content-type", "application/zip");
        String downloadFilename = new String(filename.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
        response.setContentType("application/force-download");
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");//设置允许跨域的key
        response.setHeader("Content-Disposition", "attachment;filename=" + downloadFilename);
        if (file.exists()) {
            System.out.println("文件存在");
            byte[] buffer = new byte[1024];
            try {
                FileInputStream fis = new FileInputStream(file);
                BufferedInputStream bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                //bis.close();
                //fis.close();
            } catch (Exception  e){
                e.printStackTrace();
            }

        }
    }
    public void downloadLocal(String fileUrl,String fileName,HttpServletResponse response) throws FileNotFoundException {
        // 下载本地文件
        String url = PATH + fileUrl;
        // 读到流中
        InputStream inStream = new FileInputStream(url);// 文件的存放路径
        // 设置输出的格式
        response.reset();
        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment; filename=" + fileName + "");
        // 循环取出流中的数据
        byte[] b = new byte[100];
        int len;
        try {
            while ((len = inStream.read(b)) > 0) {
                response.getOutputStream().write(b, 0, len);
            }
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

