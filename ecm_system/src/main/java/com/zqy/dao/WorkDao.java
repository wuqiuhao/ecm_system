package com.zqy.dao;

import com.zqy.entity.Course;
import com.zqy.entity.Work;

import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/23/19:14
 * @Description:
 */
public interface WorkDao {

    public int create(Work work);

    public List<Work> queryWorkByTeacherId(Work work);

    public List<Work> queryWorkByCourseId(Work work);

    public int delete(Map<String, Object> paramMap);

    public Work detail(Map<String, Object> paramMap);
}
