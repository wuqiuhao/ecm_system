package com.zqy.dao;

import com.zqy.entity.TestScoreView;

import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/02/08/16:53
 * @Description:
 */
public interface TestScoreViewDao {

    public List<TestScoreView> query(Map<String, Object> paramMap);

    public int count(Map<String, Object> paramMap);
}
