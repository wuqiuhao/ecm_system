package com.zqy.dao;

import com.zqy.entity.Course;
import com.zqy.entity.CourseResources;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/21/21:11
 * @Description:
 */
public interface ResourcesDao {

    public List<CourseResources> queryResourcesById(CourseResources courseResources);

    public int countResources(Integer courseId);

    public int delete(Integer id);

    public int create(CourseResources courseResources);

    public CourseResources detail(Integer id);

    public List<CourseResources> queryStudentCourse(Integer studentId);

    public int countStudentResources(Integer studentId);
}
