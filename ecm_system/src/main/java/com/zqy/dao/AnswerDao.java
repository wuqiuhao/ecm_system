package com.zqy.dao;

import com.zqy.entity.Answer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/24/15:52
 * @Description:
 */
public interface AnswerDao {

    public int create(Answer answer);

    public List<Answer> queryAnswerByWorkId(Integer workId);

    public Answer detail(Integer id);

    public Answer scoreDetail(@Param("workId")Integer workId,@Param("studentId") Integer studentId);


    public int score(Answer answer);

    public int check(@Param("workId")Integer workId,@Param("studentId") Integer studentId);

    public Answer checkScore(@Param("workId")Integer workId,@Param("studentId") Integer studentId);
}
