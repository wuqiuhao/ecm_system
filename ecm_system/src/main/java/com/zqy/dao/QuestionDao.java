package com.zqy.dao;

import com.zqy.entity.Paper;
import com.zqy.entity.Question;

import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/21/14:07
 * @Description:
 */
public interface QuestionDao {

    public int create(Question ques);

    public int delete(Map<String, Object> paramMap);

    public int update(Map<String, Object> paramMap);

    public List<Question> query(Map<String, Object> paramMap);

    public Question detail(Map<String, Object> paramMap);

    public int count(Map<String, Object> paramMap);

    public List<Question> queryQuestionByPaperId(Integer paperId);
}
