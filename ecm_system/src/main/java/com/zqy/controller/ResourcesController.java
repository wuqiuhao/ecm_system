package com.zqy.controller;

import com.zqy.entity.*;
import com.zqy.service.CourseService;
import com.zqy.service.ResourcesService;
import com.zqy.utils.MapControll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/21/21:05
 * @Description:
 */
@Controller
@RequestMapping("/resources")
public class ResourcesController {

    private static final String LIST = "resources/list";
    private static final String UPLOAD = "resources/upload";

    private static final String STUDENT_LIST = "resources/student_list";
    private static final String STUDENT_RESOURCES = "resources/student_resources";

    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private CourseService courseService;

    @GetMapping("/list")
    public String list(){
        return LIST;
    }

    @GetMapping("/upload")
    public String upload(){
        return UPLOAD;
    }

    @GetMapping("/student_list")
    public String listStudent(){
        return STUDENT_LIST;
    }

    @GetMapping("/student_resources")
    public String uploadStudent(){
        return STUDENT_RESOURCES;
    }

    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query(@RequestBody Course course, HttpSession session){
        //获取Teacher
        Teacher param = (Teacher) session.getAttribute("user");
        course.setTeacherId(param.getId());
        List<Course> list = courseService.queryCoursesByTeacherId(course);
        return MapControll.getInstance().success().page(list,list.size()).getMap();
    }

    @PostMapping("/upload")
    @ResponseBody
    public Map<String,Object> upload(@RequestParam("file") MultipartFile file, HttpServletRequest request){
        String base = request.getServletContext().getRealPath("/upload");
        try {
            String fileName = file.getOriginalFilename();
            String ext = fileName.substring(fileName.lastIndexOf("."),fileName.length());
            String path= UUID.randomUUID().toString()+ext;
            System.out.println(base+"/"+path);
            File newFile=new File(base+"/"+path);
            file.transferTo(newFile);
            return  MapControll.getInstance().success().add("data",path).getMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  MapControll.getInstance().error().getMap();
    }

    /**
     * 查询课程所有资源
     */
    @PostMapping("/queryInfoByCourseId/{courseId}")
    @ResponseBody
    public Map<String,Object> queryInfoByCourseId(@PathVariable("courseId") Integer courseId,@RequestBody CourseResources courseResources1){
        courseResources1.setCourseId(courseId);
        List<CourseResources> courseResources = resourcesService.queryInfoByCourseId(courseResources1);
        int count = resourcesService.countResources(courseId);
        return MapControll.getInstance().success().page(courseResources,count).getMap();
    }

    /**
     *
     */

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    @ResponseBody
    public Map<String,Object> delete(@PathVariable("id") Integer id){
        int result = resourcesService.delete(id);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody CourseResources resources, HttpSession session){

        int result = resourcesService.create(resources);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/queryByStudentId")
    @ResponseBody
    public Map<String,Object> queryByStudentId(HttpSession session){
        Student param = (Student) session.getAttribute("user");
        Integer studentId = param.getId();
        List<CourseResources> courseResources = resourcesService.queryStudentCourse(studentId);
        int count = resourcesService.countStudentResources(studentId);
        return MapControll.getInstance().success().page(courseResources,count).getMap();
    }

    @RequestMapping(value = "/getProjectFiles/{resourcesId}",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getFileInfo(@PathVariable("resourcesId") Integer resourcesId){
        CourseResources resources = resourcesService.detail(resourcesId);
        String fileName = resources.getFileName();
        String downUrl = resources.getFileUrl();
        Map fileMap = new HashMap();
        fileMap.put("fileUrl",downUrl);
        fileMap.put("fileName",fileName);
        return MapControll.getInstance().success().add(fileMap).getMap();
    }
}
