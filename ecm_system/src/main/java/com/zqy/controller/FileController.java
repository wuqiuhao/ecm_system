package com.zqy.controller;

import com.zqy.entity.CourseResources;
import com.zqy.service.RequestService;
import com.zqy.service.ResourcesService;
import com.zqy.utils.AppFileUtils;
import com.zqy.utils.MapControll;
import com.zqy.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/22/11:05
 * @Description:
 */
@Controller
@RequestMapping("/file")
public class FileController {

    @Autowired
    private ResourcesService resourcesService;
    /**
     * 添加
     *
     * @throws IOException
     * @throws IllegalStateException
     */
    @RequestMapping("/uploadFile")
    @ResponseBody
    public Map<String, Object> uploadFile(@RequestParam("file") MultipartFile mf) throws IOException {
        try {
            // 文件上传的父目录
            String parentPath = AppFileUtils.PATH;
            // 得到当前日期作为文件夹名称，这用随机数的类给下载的文件夹取新名字
            String dirName = RandomUtils.getCurrentDateForString();
            // 构造文件夹对象
            File dirFile = new File(parentPath, dirName);
            if (!dirFile.exists()) {
                dirFile.mkdirs();// 创建文件夹
            }
            // 得到文件原名
            String oldName = mf.getOriginalFilename();
            // 根据文件原名得到新名，这用随机数的类给下载的文件取新名字
            String newName = RandomUtils.createFileNameUseTime(oldName);
            File dest = new File(dirFile, newName);
            //把文件下载好了
            mf.transferTo(dest);
            return  MapControll.getInstance().success().add("data",dirName+"/"+newName).getMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  MapControll.getInstance().error().getMap();
    }

    /**
     * 下载文件
     *
     * @param
     * @param response
     * @return
     */
    @RequestMapping("/downloadFile")
    public void downloadFile(HttpServletRequest request,HttpServletResponse response) {
        String fileName = request.getParameter("fileName");
        String downUrl = request.getParameter("fileUrl");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/x-msdownload");

        String[] suffix = downUrl.split("\\.");
        try {
            String encodenickname = URLEncoder.encode(fileName+"."+suffix[1], "UTF-8");//转Unicode不然ie会乱码
            response.setHeader("Content-Disposition", "attachment;fileName=" + new String(encodenickname.getBytes("UTF-8"), "ISO8859-1"));
            File file=new File(AppFileUtils.PATH,downUrl);
            if (!file.exists()) {
                response.sendError(404, "File not found!");
                return;
            }
            long fileLength = file.length();
            response.setHeader("Content-Length", String.valueOf(fileLength));
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
            byte[] buff = new byte[2048];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            bis.close();
            bos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

