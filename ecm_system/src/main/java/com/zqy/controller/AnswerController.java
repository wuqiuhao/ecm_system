package com.zqy.controller;

import com.zqy.entity.*;
import com.zqy.service.AnswerService;
import com.zqy.utils.MapControll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/24/11:41
 * @Description:
 */
@Controller
@RequestMapping("/answer")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    private static final String LIST = "answer/list";
    private static final String ADD = "answer/add";
    private static final String UPDATE = "answer/update";
    private static final String AUDIT = "answer/audit";
    private static final String ANSWER_DETAIL = "answer/detail";


    @GetMapping("/add")
    public String create(){
        return ADD;
    }

    @GetMapping("/list")
    public String list(){
        return LIST;
    }

    @GetMapping("/update")
    public String update(){
        return UPDATE;
    }

    @GetMapping("/audit")
    public String audit(){
        return AUDIT;
    }

    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody Answer answer, HttpSession session){
        Student param = (Student) session.getAttribute("user");
        answer.setStudentId(param.getId());
        int result = answerService.create(answer);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/queryAnswerByWorkId/{workId}")
    @ResponseBody
    public Map<String,Object> queryAnswerByWorkId(@PathVariable("workId") Integer workId, HttpSession session){
        List<Answer> list = answerService.queryAnswerByWorkId(workId);
        return MapControll.getInstance().success().page(list,list.size()).getMap();
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Integer id, ModelMap modelMap){
        Answer answer = answerService.detail(id);
        modelMap.addAttribute("answer",answer);
        return ANSWER_DETAIL;
    }



    @PostMapping("/score")
    @ResponseBody
    public Map<String,Object> score(@RequestBody Answer answer){
        int result = answerService.score(answer);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/check/{id}")
    @ResponseBody
    public Map<String,Object> check(@PathVariable("id")Integer workId,HttpSession session){
        Student param = (Student) session.getAttribute("user");
        int result = answerService.check(workId,param.getId());
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/checkScore/{id}")
    @ResponseBody
    public Map<String,Object> checkScore(@PathVariable("id")Integer workId,HttpSession session){
        Student param = (Student) session.getAttribute("user");
        Answer result = answerService.checkScore(workId,param.getId());
        return MapControll.getInstance().success().add("data",result).getMap();
    }
}
