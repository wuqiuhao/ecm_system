package com.zqy.controller;

import com.zqy.entity.*;
import com.zqy.service.PaperService;
import com.zqy.service.QuestionService;
import com.zqy.utils.MapControll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/21/14:40
 * @Description:
 */
@Controller
@RequestMapping("/question")
public class QuestionController {

    private static final String LIST = "question/list";
    private static final String ADD = "question/add";
    private static final String UPDATE = "question/update";

    @Autowired
    private QuestionService questionService;

    @Autowired
    private PaperService paperService;

    @GetMapping("/list")
    public String list(){
        return LIST;
    }

    @GetMapping("/add")
    public String create(ModelMap modelMap){
        List<Question> questions = questionService.query(null);
        modelMap.addAttribute("questions",questions);
        return ADD;
    }

    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody Question question){
        int result = questionService.create(question);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/delete/{id}")
    @ResponseBody
    public Map<String,Object> delete(@PathVariable("id") Integer id){
        int result = questionService.delete(id);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/delete")
    @ResponseBody
    public Map<String,Object> delete(String ids){
        int result = questionService.delete(ids);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/update")
    @ResponseBody
    public Map<String,Object> update(@RequestBody Question question){
        int result = questionService.update(question);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }

    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query(@RequestBody Question question, HttpSession session){
        //获取Teacher
        Teacher param = (Teacher) session.getAttribute("user");
        question.setTeacherId(param.getId());
        List<Question> list = questionService.query(question);
        Integer count = questionService.count(question);
        return MapControll.getInstance().success().add("data",list).getMap();
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Integer id, ModelMap modelMap){
        Question question = questionService.detail(id);
        List<Question> questions = questionService.query(null);
        modelMap.addAttribute("question",question);
        modelMap.addAttribute("questions",questions);
        return UPDATE;
    }

    @GetMapping("/queryPaperById")
    @ResponseBody
    public List<Paper> queryPaperById(HttpSession session){
        //获取Teacher
        Teacher param = (Teacher) session.getAttribute("user");
        List<Paper> list = paperService.queryPaperById(param.getId());
        return list;
    }
}
