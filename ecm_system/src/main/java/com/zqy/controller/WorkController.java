package com.zqy.controller;

import com.zqy.entity.*;
import com.zqy.service.AnswerService;
import com.zqy.service.CourseService;
import com.zqy.service.WorkService;
import com.zqy.utils.MapControll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/23/16:43
 * @Description:
 */
@Controller
@RequestMapping("/work")
public class WorkController {

    @Autowired
    private WorkService workService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private AnswerService answerService;

    private static final String LIST = "work/list";
    private static final String ADD = "work/add";
    private static final String UPDATE = "work/update";
    private static final String AUDIT = "answer/audit";
    private static final String STUDENT_WORK = "work/student_work";
    private static final String SCORE = "answer/score";

    @GetMapping("/add")
    public String create(){
        return ADD;
    }

    @GetMapping("/list")
    public String list(){
        return LIST;
    }

    @GetMapping("/update")
    public String update(){
        return UPDATE;
    }

    @GetMapping("/audit")
    public String audit(){
        return AUDIT;
    }

    @GetMapping("/student_work")
    public String student_work(){
        return STUDENT_WORK;
    }

    @PostMapping("/create")
    @ResponseBody
    public Map<String,Object> create(@RequestBody Work work,HttpSession session){
        //获取Teacher
        Teacher param = (Teacher) session.getAttribute("user");
        work.setTeacherId(param.getId());
        int result = workService.create(work);
        if(result <= 0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }


    @PostMapping("/queryWorkByCourseId/{courseId}")
    @ResponseBody
    public Map<String, Object> queryWorkByCourseId(@PathVariable("courseId")Integer courseId, HttpSession session){
        Student param = (Student) session.getAttribute("user");
        Work work = new Work();
        work.setStudentId(param.getId());
        work.setCourseId(courseId);
        List<Work> list = workService.queryWorkByCourseId(work);
        return MapControll.getInstance().success().page(list,list.size()).getMap();
    }

    @GetMapping("/scoreDetail/{id}")
    public String scoreDetail(@PathVariable("id") Integer id, ModelMap modelMap,HttpSession session){
        Student param = (Student) session.getAttribute("user");
        Answer answer = answerService.scoreDetail(id,param.getId());
        modelMap.addAttribute("answer",answer);
        return SCORE;
    }

    @PostMapping("/queryCourse")
    @ResponseBody
    public Map<String, Object> queryCourse(HttpSession session){
        //获取Teacher
        Course course = new Course();
        Teacher param = (Teacher) session.getAttribute("user");
        course.setTeacherId(param.getId());
        List<Course> list = courseService.queryCoursesByTeacherId(course);
        return MapControll.getInstance().success().page(list,null).getMap();
    }

    @PostMapping("/query")
    @ResponseBody
    public Map<String,Object> query(@RequestBody Work work, HttpSession session){
        //获取Teacher
        Teacher param = (Teacher) session.getAttribute("user");
        work.setTeacherId(param.getId());
        List<Work> list = workService.queryWorkByTeacherId(work);
        return MapControll.getInstance().success().page(list,list.size()).getMap();
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Integer id, ModelMap modelMap){
        Work work = workService.detail(id);
        modelMap.addAttribute("work",work);
        return UPDATE;
    }

    @GetMapping("/workDetail/{id}")
    public String workDetail(@PathVariable("id") Integer id, ModelMap modelMap){
        Work work = workService.detail(id);
        modelMap.addAttribute("work",work);
        return AUDIT;
    }


    @PostMapping("/delete")
    @ResponseBody
    public Map<String,Object> delete(String ids){
        int result = workService.delete(ids);
        if(result<=0){
            return MapControll.getInstance().error().getMap();
        }
        return MapControll.getInstance().success().getMap();
    }
}
