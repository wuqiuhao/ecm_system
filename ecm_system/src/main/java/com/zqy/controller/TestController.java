package com.zqy.controller;

import com.zqy.entity.Paper;
import com.zqy.entity.Question;
import com.zqy.entity.Student;
import com.zqy.entity.TestScoreView;
import com.zqy.service.PaperService;
import com.zqy.service.QuestionService;
import com.zqy.service.TestScoreViewService;
import com.zqy.utils.MapControll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/25/11:28
 * @Description:
 */
@Controller
@RequestMapping("/test")
public class TestController {

    private static final String LIST = "test/list";

    private static final String DETAIL = "test/testing";

    @Autowired
    private PaperService paperService;

    @Autowired
    private TestScoreViewService testScoreViewService;

    @Autowired
    private QuestionService questionService;

    @GetMapping("/list")
    public String list(){
        return LIST;
    }

    @GetMapping("/testing")
    public String testing(){
        return DETAIL;
    }

    @PostMapping("/queryPaperByStuId")
    @ResponseBody
    public Map<String, Object> queryPaperByStuId(@RequestBody Paper paper,HttpSession session){

        Student param = (Student) session.getAttribute("user");
        TestScoreView testScoreView = new TestScoreView();
        testScoreView.setPaperName(paper.getPaperName());
        testScoreView.setStuId(param.getId());
        List<TestScoreView> list = testScoreViewService.query(testScoreView);

        paper.setStudentId(param.getId());
        int count = testScoreViewService.count(testScoreView);
        return MapControll.getInstance().success().page(list,count).getMap();
    }

    @GetMapping("/getRandTopic")
    @ResponseBody
    public List<Question> getRandTopic(Integer paperId){
       List<Question> list = questionService.queryQuestionByPaperId(paperId);
       return list;
    }
}
