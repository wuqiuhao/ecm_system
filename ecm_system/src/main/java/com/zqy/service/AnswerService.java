package com.zqy.service;

import com.zqy.dao.AnswerDao;
import com.zqy.entity.Answer;
import com.zqy.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/24/15:52
 * @Description:
 */
@Service
public class AnswerService {

    @Autowired
    private AnswerDao answerDao;

    public int create(Answer answer) {
        return answerDao.create(answer);
    }

    public List<Answer> queryAnswerByWorkId(Integer workId){
        return answerDao.queryAnswerByWorkId(workId);
    }

    public Answer detail(Integer id){
        return answerDao.detail(id);
    }

    public Answer scoreDetail(Integer id,Integer studentId){
        return answerDao.scoreDetail(id,studentId);
    }

    public int score(Answer answer){
        return answerDao.score(answer);
    }

    public int check(Integer workId,Integer studentId){
        return answerDao.check(workId,studentId);
    }

    public Answer checkScore(Integer workId,Integer studentId){
        return answerDao.checkScore(workId,studentId);
    }
}
