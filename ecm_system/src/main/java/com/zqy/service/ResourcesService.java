package com.zqy.service;

import com.github.pagehelper.PageHelper;
import com.zqy.dao.CourseDao;
import com.zqy.dao.ResourcesDao;
import com.zqy.entity.Course;
import com.zqy.entity.CourseResources;
import com.zqy.utils.BeanMapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/21/21:10
 * @Description:
 */
@Service
public class ResourcesService {

    @Autowired
    private ResourcesDao resourcesDao;

    private CourseDao courseDao;

    public List<Course> query(Course course) {
        if(course != null && course.getPage() != null){
            PageHelper.startPage(course.getPage(),course.getLimit());
        }
        return courseDao.queryCoursesById(BeanMapUtils.beanToMap(course));
    }

    public List<CourseResources> queryInfoByCourseId(CourseResources courseResources){
        return resourcesDao.queryResourcesById(courseResources);
    }

    public int countResources(Integer courseId){
        return resourcesDao.countResources(courseId);
    }

    public int delete(Integer id){
        return resourcesDao.delete(id);
    }

    public int create(CourseResources resources) {
        return resourcesDao.create(resources);
    }

    public CourseResources detail(Integer id){
        return resourcesDao.detail(id);
    }

    public List<CourseResources> queryStudentCourse(Integer studentId){
        return resourcesDao.queryStudentCourse(studentId);
    }

    public int countStudentResources(Integer studentId){
        return resourcesDao.countStudentResources(studentId);
    }
}
