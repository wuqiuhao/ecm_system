package com.zqy.service;

import com.github.pagehelper.PageHelper;
import com.zqy.dao.TestScoreViewDao;
import com.zqy.entity.TestScoreView;
import com.zqy.utils.BeanMapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/02/08/16:51
 * @Description:
 */
@Service
public class TestScoreViewService {

    @Autowired
    private TestScoreViewDao testScoreViewDao;

    public List<TestScoreView> query(TestScoreView testScoreView){
        if(testScoreView != null && testScoreView.getPage() != null){
            PageHelper.startPage(testScoreView.getPage(),testScoreView.getLimit());
        }
        return testScoreViewDao.query(BeanMapUtils.beanToMap(testScoreView));
    }

    public int count(TestScoreView testScoreView){
        return testScoreViewDao.count(BeanMapUtils.beanToMap(testScoreView));
    }
}
