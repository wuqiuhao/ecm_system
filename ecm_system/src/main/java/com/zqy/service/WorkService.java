package com.zqy.service;

import com.zqy.dao.CourseDao;
import com.zqy.dao.WorkDao;
import com.zqy.entity.Course;
import com.zqy.entity.Work;
import com.zqy.utils.MapParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wuQiuHao
 * @Date: 2022/05/23/19:12
 * @Description:
 */
@Service
public class WorkService {

    @Autowired
    private WorkDao workDao;

    public int create(Work work) {
        return workDao.create(work);
    }

    public List<Work> queryWorkByTeacherId(Work work){
        return workDao.queryWorkByTeacherId(work);
    }

    public List<Work> queryWorkByCourseId(Work work){
        return workDao.queryWorkByCourseId(work);
    }

    public int delete(String ids) {
        int flag = 0;
        for (String str : ids.split(",")) {
            flag = workDao.delete(MapParameter.getInstance().addId(Integer.parseInt(str)).getMap());
        }
        return flag;
    }

    public Work detail(Integer id) {

        return workDao.detail(MapParameter.getInstance().addId(id).getMap());
    }
}
