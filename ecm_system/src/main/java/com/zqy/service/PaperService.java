package com.zqy.service;

import com.github.pagehelper.PageHelper;
import com.zqy.dao.PaperDao;
import com.zqy.entity.Clazz;
import com.zqy.entity.Paper;
import com.zqy.utils.BeanMapUtils;
import com.zqy.utils.MapParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/17/14:11
 * @Description:
 */
@Service
public class PaperService {

    @Autowired
    private PaperDao paperDao;

    public int create(Paper pi) {
        return paperDao.create(pi);
    }

    public int delete(Integer id) {
        return paperDao.delete(MapParameter.getInstance().addId(id).getMap());
    }

    public int delete(String ids) {
        int flag = 0;
        for (String str : ids.split(",")) {
            flag = paperDao.delete(MapParameter.getInstance().addId(Integer.parseInt(str)).getMap());
        }
        return flag;
    }

    public int update(Paper paper) {
        Map<String, Object> map = MapParameter.getInstance().add(BeanMapUtils.beanToMapForUpdate(paper)).addId(paper.getId()).getMap();
        return paperDao.update(map);
    }

    public List<Paper> query(Paper paper) {
        if(paper != null && paper.getPage() != null){
            PageHelper.startPage(paper.getPage(),paper.getLimit());
        }
        return paperDao.query(BeanMapUtils.beanToMap(paper));
    }

    public int updateStatus(Integer paperId,Integer status){
        Map<String,Object> map = MapParameter.getInstance()
                .add("paperId",paperId)
                .add("status",status)
                .getMap();
        return paperDao.updateStatus(map);
    }

    public Paper detail(Integer id) {
        return paperDao.detail(MapParameter.getInstance().addId(id).getMap());
    }

    public int count(Paper paper) {
        return paperDao.count(BeanMapUtils.beanToMap(paper));
    }

    public List<Paper> queryPaperById(Integer teacherId){
        return paperDao.queryPaperById(teacherId);
    }

    public List<Paper> queryPaperByStuId(Integer studentId){
        return paperDao.queryPaperByStuId(studentId);
    }

    public int countPaper(Paper paper){return paperDao.countPaper(BeanMapUtils.beanToMap(paper));}
}
