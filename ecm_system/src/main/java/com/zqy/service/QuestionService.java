package com.zqy.service;

import com.github.pagehelper.PageHelper;
import com.zqy.dao.PaperDao;
import com.zqy.dao.QuestionDao;
import com.zqy.entity.Paper;
import com.zqy.entity.Question;
import com.zqy.utils.BeanMapUtils;
import com.zqy.utils.MapParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: wuQiuHao
 * @Date: 2022/01/21/14:38
 * @Description:
 */
@Service
public class QuestionService {

    @Autowired
    private QuestionDao questionDao;

    public int create(Question pi) {
        return questionDao.create(pi);
    }

    public int delete(Integer id) {
        return questionDao.delete(MapParameter.getInstance().addId(id).getMap());
    }

    public int delete(String ids) {
        int flag = 0;
        for (String str : ids.split(",")) {
            flag = questionDao.delete(MapParameter.getInstance().addId(Integer.parseInt(str)).getMap());
        }
        return flag;
    }

    public int update(Question question) {
        Map<String, Object> map = MapParameter.getInstance().add(BeanMapUtils.beanToMapForUpdate(question)).addId(question.getId()).getMap();
        return questionDao.update(map);
    }

    public List<Question> query(Question question) {
        if(question != null && question.getPage() != null){
            PageHelper.startPage(question.getPage(),question.getLimit());
        }
        return questionDao.query(BeanMapUtils.beanToMap(question));
    }

    public Question detail(Integer id) {
        return questionDao.detail(MapParameter.getInstance().addId(id).getMap());
    }

    public int count(Question question) {
        return questionDao.count(BeanMapUtils.beanToMap(question));
    }

    public List<Question> queryQuestionByPaperId(Integer paperId){
       return questionDao.queryQuestionByPaperId(paperId);
    }
}
